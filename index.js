const args = require('yargs').argv;
const bets_on_zero = args.bets_on_zero ? true : false;

if (args.bet && args.balance) {
    start_loop(args.bet, args.balance, args.bet, args.balance, bets_on_zero);
}

function start_loop(bet, balance, initial_bet, initial_balance, bets_on_zero) {
    const balance_sheet = { bet, balance, initial_balance, initial_bet, rounds_played: 0, bets_on_zero };
    let result_log = [];

    while (balance_sheet.balance > 0 && balance_sheet.balance < balance_sheet.initial_balance * 1.2) {
        play_round(balance_sheet, bets_on_zero, result_log);
    }

    console.log(result_log);
    if (balance_sheet.balance == 0) {
        console.log("lost all money");
    } else {
        console.log(`Won in ${balance_sheet.rounds_played} rounds`);
    }
}

function play_round(balance_sheet, bets_on_zero, result_log) {
    let info;
    let bet = balance_sheet.bet;
    if (bet > balance_sheet.balance) {
        bet = balance_sheet.balance;
    }
    balance_sheet.balance -= bet;
    const round_result = determine_outcome(bet, bets_on_zero);
    balance_sheet.balance += round_result;
    if (round_result == 0) {
        balance_sheet.bet *= 2;
    } else {
        balance_sheet.bet = balance_sheet.initial_bet;
    }

    if (round_result === 0) {
        info = `Lost ${bet}, balance: ${balance_sheet.balance}`;
    } else {
        info = `Won ${round_result}, balance: ${balance_sheet.balance}`;
    }
    result_log.push(info);
    balance_sheet.rounds_played++;
}

function determine_outcome(bet, bets_on_zero) {
    const roll = Math.floor(Math.random() * 38);
    const isRed = roll <= 17;

    if (bets_on_zero === true) {
        const isNull = (roll === 0);
        return isNull ? (bet * 35) : 0;
    } else {
        return isRed ? (bet * 2) : 0;
    }
}